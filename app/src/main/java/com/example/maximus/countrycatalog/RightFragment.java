package com.example.maximus.countrycatalog;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class RightFragment extends Fragment {

    private static RightFragment instance;


    TextView name;
    TextView region;

    public static RightFragment getInstance() {
        if (instance == null) {
            instance = new RightFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        name = (TextView) root.findViewById(R.id.note_name);
        region = (TextView) root.findViewById(R.id.note_info);

        name.setText(LeftFragment.getInstance().adapter.getItem(0).getName());
        region.setText(LeftFragment.getInstance().adapter.getItem(0).getRegion());

        return root;
    }
}
