package com.example.maximus.countrycatalog.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.maximus.countrycatalog.R;
import com.example.maximus.countrycatalog.country.Country;

import java.util.List;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Country> countries;
    private OnRecyclItemClic onRecyclItemClic;

    public MyRecyclerViewAdapter(Context context, List<Country> countries, OnRecyclItemClic onRecyclItemClic) {
        this.context = context;
        this.countries = countries;
        this.onRecyclItemClic = onRecyclItemClic;
    }

    public interface OnRecyclItemClic {
        void onClick(int position);
    }

    public void addNote(Country country) {
        countries.add(1, country);
        notifyItemInserted(1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.country_info, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Country country = countries.get(position);
        holder.name.setText(country.getName());
        holder.region.setText(country.getRegion());

//                .substring(0, 14));

    }

    public void deleteNote(int position) {
        if (position >= 0 && position < countries.size()) {
            countries.remove(position);
            notifyItemRangeRemoved(position, 1);
        }
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public Country getItem(int position) {
        return countries.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView region;

        public ViewHolder(View item) {
            super(item);
            name = (TextView) item.findViewById(R.id.note_name);
            region = (TextView) item.findViewById(R.id.note_info);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClic.onClick(getAdapterPosition());
                }
            });
        }

    }
}
