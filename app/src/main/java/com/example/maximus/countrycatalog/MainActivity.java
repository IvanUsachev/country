package com.example.maximus.countrycatalog;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, LeftFragment.getInstance())
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_right, RightFragment.getInstance())
                .commit();

    }
}

