package com.example.maximus.countrycatalog.country;

public class Country {
    String name;
    String capital;
    String region;
    String subregion;
    int population;
    int area;
    String timezones;
//    Object flag;

    public Country(String name, String capital, String region, String subregion, int population, int area, String timezones) {
        this.name = name;
        this.capital = capital;
        this.region = region;
        this.subregion = subregion;
        this.population = population;
        this.area = area;
        this.timezones = timezones;
//        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public int getPopulation() {
        return population;
    }

    public int getArea() {
        return area;
    }

    public String getTimezones() {
        return timezones;
    }

//    public Object getFlag() {
//        return flag;
//    }
}
